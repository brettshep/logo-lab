import { LabGuard } from "./lab.guard";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

export enum CompName {
  home = 1,
  lab = 2
}

const routes: Routes = [
  {
    path: "home",
    data: { name: CompName.home },
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path: "lab",
    data: { name: CompName.lab },
    loadChildren: () => import('./lab/lab.module').then(m => m.LabModule),
    canActivate: [LabGuard]
  },
  {
    path: "terms",
    loadChildren:
      () => import('./terms-conditions/terms-conditions.module').then(m => m.TermsConditionsModule)
  },
  {
    path: "privacy-policy",
    loadChildren: () => import('./privacy-policy/privacy-policy.module').then(m => m.PrivacyPolicyModule)
  },
  { path: "**", redirectTo: "home", pathMatch: "full" }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
