import { Router } from '@angular/router';
import { Store } from './../store';
import { ImageService } from './../image.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <div class="wrapper">
      <!-- <a
        class="banner"
        target="_blank"
        href="https://www.youtube.com/@BrandonShep"
        ><img src="assets/youtube.png" alt="youtube"
      /></a> -->
      <div class="adobe-banner">
        <button (click)="openAdobe()">
          <img
            class="outer"
            src="/assets/adobeBanner.png"
            width="728"
            height="90"
            border="0"
          />
          <div class="inner">
            <img
              src="/assets/adobeBannerInner.png"
              width="1007"
              height="60"
              border="0"
            />
            <img
              src="/assets/adobeBannerInner.png"
              width="1007"
              height="60"
              border="0"
            />
          </div>
        </button>
      </div>

      <div class="content">
        <img class="logo" src="/assets/logo.png" alt="logo" />
        <a target="_blank" href="https://brandon.design"
          >Made with <i class="fas fa-heart"></i> by Brandon Shepherd.</a
        >
        <h3>
          Put your logo to the test and find out where it succeeds and where
          improvements could be made.
        </h3>
        <div class="uploadCont">
          <button>UPLOAD A LOGO</button>
          <input type="file" (change)="handleImgUpload($event)" title=" " />
        </div>
        <div class="err" *ngIf="error">{{ error }}</div>
        <button class="sample" (click)="createSample()">Try a sample</button>
      </div>
      <div class="bg">
        <img src="/assets/home-page.png" alt="background" />
      </div>
    </div>
  `,
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent {
  error: string = '';
  sampleSrc = '/assets/sampleLogo.png';
  constructor(
    private imgServ: ImageService,
    private store: Store,
    private router: Router
  ) {}

  openAdobe() {
    //open link in new tab
    window.open('https://bit.ly/AdobeStockTrial');
  }

  async handleImgUpload(e, directFile?: boolean) {
    try {
      const { orig, sized } = await this.imgServ.extractFile(e, directFile);
      //set in store
      this.store.set('origFile', orig);
      this.store.set('sizedFile', sized);

      //clear error
      this.error = '';

      //navigate
      this.router.navigate(['/lab']);
    } catch (e) {
      this.error = e;
    }
  }

  createSample() {
    fetch(this.sampleSrc)
      .then((val) => {
        val.blob().then((val) => this.handleImgUpload(val, true));
      })
      .catch((err) => (this.error = err));
  }
}
