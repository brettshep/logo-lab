import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ImageService {
  // constructor(private sanitizer: DomSanitizer) {}

  extractFile(e, directFile?: boolean): Promise<{ orig: Blob; sized: Blob }> {
    let file: File;
    if (!directFile) {
      const files = e.target.files;
      if (files && files[0]) {
        file = files[0];
      } else return Promise.reject("No File Found!");
    } else {
      file = e;
    }
    //see if image file
    let check = this.checkType(file);
    if (check === true) {
      return this.testSvg(file);
    } else {
      return Promise.reject(check);
    }
  }

  checkType(file: File): boolean | string {
    if (file.type.match("image/png") || file.type.match("image/svg")) {
      return true;
    } else {
      return "File must be a PNG or SVG!";
    }
  }

  //test svg and fix if needed
  testSvg(file: File): Promise<{ orig: Blob; sized: Blob }> {
    //-----if image is SVG-----
    return new Promise((resolve, reject) => {
      if (file.type.match(/(image\/svg*)/)) {
        let reader = new FileReader();
        //on reader load
        reader.onload = () => {
          //get root svg tag
          let res = (reader.result as string).match(/<svg([^>]+)>/);
          if (res) {
            //svg line as string
            let line = res[1];
            let viewboxStr = line.match(/viewBox\s*=\s*".*?"/);
            //if viewbox exist
            if (viewboxStr) {
              let viewBoxNumsArr = viewboxStr[0].match(/\d+\.?\d*/g);
              let width =
                parseInt(viewBoxNumsArr[2]) - parseInt(viewBoxNumsArr[0]);
              let height =
                parseInt(viewBoxNumsArr[3]) - parseInt(viewBoxNumsArr[1]);
              //resize so longest size is 450
              if (height !== 450 || width !== 450) {
                if (width >= height) {
                  height *= 450 / width;
                  width = 450;
                } else {
                  if (height > 450) {
                    width *= 450 / height;
                    height = 450;
                  }
                }
              }

              //if no height or width props
              if (!/width|height/.test(line)) {
                line += ` width='${width}px' height='${height}px'`;
              }
              //replace width and height with calculated width and height
              else {
                line = line.replace(/width\s*=\s*".*?"/, ` width='${width}px'`);
                line = line.replace(
                  /height\s*=\s*".*?"/,
                  ` height='${height}px'`
                );
              }
            }
            //NO VIEWBOX!
            else {
              reject("Please upload a valid SVG!");
            }

            //replace root svg line
            let replaced = (reader.result as string).replace(
              /<svg([^>]+)>/,
              `<svg ${line} >`
            );
            //create blob from svg string
            const blob = new Blob([replaced], { type: "image/svg+xml" });

            resolve({ orig: blob, sized: blob });
          }
        };
        //set file in filereader
        reader.readAsText(file);
      }
      // -----image NOT SVG-----
      else {
        //resize file and set image
        this.resizeImage({ file, maxSize: 450 }).then(val => {
          resolve({ orig: file, sized: val });
        });
      }
    });
  }

  //resize image
  resizeImage(settings: { maxSize: number; file: File | Blob }): Promise<Blob> {
    const file = settings.file;
    const maxSize = settings.maxSize;
    const reader = new FileReader();
    const image = new Image();
    const canvas = document.createElement("canvas");
    const dataURItoBlob = (dataURI: string) => {
      const bytes =
        dataURI.split(",")[0].indexOf("base64") >= 0
          ? atob(dataURI.split(",")[1])
          : unescape(dataURI.split(",")[1]);
      const mime = dataURI
        .split(",")[0]
        .split(":")[1]
        .split(";")[0];
      const max = bytes.length;
      const ia = new Uint8Array(max);
      for (var i = 0; i < max; i++) ia[i] = bytes.charCodeAt(i);
      return new Blob([ia], { type: mime });
    };
    const resize = () => {
      let width = image.width;
      let height = image.height;

      if (width > height) {
        height *= maxSize / width;
        width = maxSize;
      } else {
        width *= maxSize / height;
        height = maxSize;
      }
      canvas.width = width;
      canvas.height = height;
      let context = canvas.getContext("2d");
      context.drawImage(image, 0, 0, width, height);
      let dataUrl = canvas.toDataURL("image/png");
      return dataURItoBlob(dataUrl);
    };

    return new Promise((ok, no) => {
      if (!file.type.match(/image.*/)) {
        no(new Error("Not an image"));
        return;
      }

      reader.onload = (readerEvent: any) => {
        image.onload = () => ok(resize());
        image.src = readerEvent.target.result;
      };
      reader.readAsDataURL(file);
    });
  }

  // sanitize(url: string): any {
  //   return this.sanitizer.bypassSecurityTrustUrl(url);
  // }
}
