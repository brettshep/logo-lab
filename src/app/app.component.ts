import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { RouteAnimations } from './routeAnimations';

@Component({
  selector: 'app-root',
  template: `
    <div class="page" [@routeAnimation]="getDepth(myOutlet)">
      <nav class="navTop nav">
        <a target="_blank" href="https://brandon.design/#products"
          >MORE TOOLS</a
        >
        <a href="mailto:hello@brandon.design">CONTACT</a>
      </nav>
      <nav class="navBottom nav">
        <a
          target="_blank"
          href="https://www.dropbox.com/sh/tyon1ppvytzk1qt/AAAcXi7ZaFD3R-sWqR0ZkhYRa?dl=0"
          >PRESS KIT</a
        >
      </nav>
      <div class="socialCont">
        <h3>SHARE ON</h3>
        <div class="socialBtnCont">
          <a
            target="_blank"
            href="https://www.facebook.com/sharer/sharer.php?u=https%3A//logolab.app"
          >
            <div id="facebook" class="share">
              <i class="fab fa-facebook-f"></i>
            </div>
          </a>
          <a
            target="_blank"
            href="https://twitter.com/home?status=https%3A//logolab.app"
          >
            <div id="twitter" class="share">
              <i class="fab fa-twitter"></i>
            </div>
          </a>
        </div>
      </div>
      <router-outlet #myOutlet="outlet"></router-outlet>
    </div>
  `,
  styleUrls: ['./app.component.sass'],
  animations: RouteAnimations,
})
export class AppComponent {
  //-------ROUTER ANIMS------
  getDepth(outlet: RouterOutlet) {
    let name = outlet.activatedRouteData['name'];
    if (name) return name;
    else return -1;
  }
}
