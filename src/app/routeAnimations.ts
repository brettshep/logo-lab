import { CompName } from "./routing.module";
import {
  trigger,
  transition,
  group,
  query,
  style,
  animate,
  state
} from "@angular/animations";

let up = [
  query(
    ":enter",
    style({
      transform: "translate3d(0,100%,0)",
      zIndex: 100,
      boxShadow: "0 -10px 30px 0 rgba(0,0,0,.1)"
    })
  ),
  query(
    ":enter , :leave",
    style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
    { optional: true }
  ),
  query(":enter", [
    animate(".5s ease", style({ transform: "translate3d(0,0,0)" }))
  ])
];

let down = [
  query(
    ":leave",
    style({ zIndex: 100, boxShadow: "0 -10px 30px 0 rgba(0,0,0,.1)" })
  ),
  query(
    ":enter , :leave",
    style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
    { optional: true }
  ),

  query(":leave", [
    animate(".5s ease", style({ transform: "translate3d(0,100%,0)" }))
  ])
];

let right = [
  query(":enter", style({ transform: "translate3d(100%,0,0)" })),
  query(
    ":enter , :leave",
    style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
    { optional: true }
  ),
  group([
    query(
      ":leave",
      [animate(".5s ease", style({ transform: "translate3d(-100%,0,0)" }))],
      { optional: true }
    ),
    query(":enter", [
      animate(".5s ease", style({ transform: "translate3d(0,0,0)" }))
    ])
  ])
];

let left = [
  query(":enter", style({ transform: "translate3d(-100%,0,0)" })),
  query(
    ":enter , :leave",
    style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
    { optional: true }
  ),
  group([
    query(
      ":leave",
      [animate(".5s ease", style({ transform: "translate3d(100%,0,0)" }))],
      { optional: true }
    ),
    query(":enter", [
      animate(".5s ease", style({ transform: "translate3d(0,0,0)" }))
    ])
  ])
];

export const FadeOut = trigger("FadeOut", [
  state(
    "notVisible",
    style({
      opacity: 0
    })
  ),
  transition("visible => notVisible", [
    style({
      opacity: 1
    }),
    animate(
      ".5s ease",
      style({
        opacity: 0
      })
    )
  ])
]);

export const RouteAnimations = [
  trigger("routeAnimation", [
    transition(`-1 => ${CompName.home}`, []),
    // ** TO LAB
    transition(`* => ${CompName.lab}`, up),
    // ** FROM DATES
    transition(`${CompName.lab} => *`, down)
    // // HOME TO LIKES
    // transition(`${CompName.home} => ${CompName.likes}`, right),
    // // HOME TO TIME
    // transition(`${CompName.home} => ${CompName.time}`, up),
    // // TIME TO LIKES
    // transition(`${CompName.time} => ${CompName.likes}`, right),
    // // LIKES TO HOME
    // transition(`${CompName.likes} => ${CompName.home}`, left),
    // // LIKES TO TIME
    // transition(`${CompName.likes} => ${CompName.time}`, left)
  ])
];

export const FadeInOut = trigger("FadeInOut", [
  transition(":enter", [
    style({
      opacity: 0
    }),
    animate(
      ".3s ease",
      style({
        opacity: 1
      })
    )
  ]),
  transition(":leave", [
    style({
      opacity: 1
    }),
    animate(
      ".3s ease",
      style({
        opacity: 0
      })
    )
  ])
]);

export const FadeInOutState = trigger("FadeInOutState", [
  state(
    "loading",
    style({
      opacity: 0
    })
  ),
  state(
    "loaded",
    style({
      opacity: 1
    })
  ),
  transition("loading => loaded", [
    style({
      opacity: 0
    }),
    animate(
      ".3s ease",
      style({
        opacity: 1
      })
    )
  ])
  // transition("loaded => loading", [
  //   style({
  //     opacity: 1
  //   }),
  //   animate(
  //     ".3s ease",
  //     style({
  //       opacity: 0
  //     })
  //   )
  // ])
]);
