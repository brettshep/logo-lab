import { first } from "rxjs/operators";
import { ImageService } from "./../image.service";
import { Store } from "../store";
import { DomSanitizer } from "@angular/platform-browser";
import { Component, OnInit } from "@angular/core";
import { FadeInOutState } from "../routeAnimations";

@Component({
  selector: "app-home",
  templateUrl: "./lab.component.html",
  styleUrls: ["./lab.component.sass"],
  animations: [FadeInOutState]
})
export class LabComponent implements OnInit {
  src: any;
  img: HTMLImageElement;
  origImg: HTMLImageElement;
  origSrc: any;
  imgFit: boolean = false;
  ready: boolean = false;
  dragHover: boolean = false;
  sectionStatus = 3;
  error: string = "";
  loadState = "loading";
  constructor(
    private store: Store,
    private imgServ: ImageService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    setTimeout(() => {
      //set orig
      this.store
        .select<Blob | File>("origFile")
        .pipe(first())
        .subscribe(val => this.setOrigImage(val));
      this.store
        .select<Blob | File>("sizedFile")
        .pipe(first())
        .subscribe(val => this.setSizedImage(val));
    }, 500);
  }

  scrollDown() {
    const elem: HTMLElement = document.querySelector("#balance-section");
    if (elem)
      elem.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "start"
      });
  }

  sectionDone() {
    this.sectionStatus--;
    if (this.sectionStatus === 0) {
      this.sectionStatus = 3;
      this.loadState = "loaded";
    }
  }

  //final set and resize image
  setSizedImage(blob: Blob) {
    this.loadState = "loading";
    //set img src
    this.src = this.sanitize(URL.createObjectURL(blob));
    //create new img and test for fit
    let image = new Image();
    image.onload = () => {
      let w = image.width;
      let h = image.height;
      if (w / h <= 2.23) this.imgFit = true;
      else this.imgFit = false;
      //set DOM ready
      this.img = image;
      this.ready = true;
    };
    image.src = this.src.changingThisBreaksApplicationSecurity;
  }

  async handleImgUpload(e, directFile?: boolean) {
    try {
      const { orig, sized } = await this.imgServ.extractFile(e, directFile);
      this.setSizedImage(sized);
      this.setOrigImage(orig);
      this.error = "";
    } catch (e) {
      this.error = e;
    }
  }

  setOrigImage(file) {
    this.origSrc = this.sanitize(URL.createObjectURL(file));
    //create new img and test for fit
    let image = new Image();
    image.onload = () => {
      //set DOM ready
      this.origImg = image;
    };
    image.src = this.origSrc.changingThisBreaksApplicationSecurity;
  }

  //drag states
  ondragover(e) {
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = "copy";
    this.dragHover = true;
  }

  ondragout(e) {
    e.preventDefault();
    this.dragHover = false;
  }

  ondrop(e: DragEvent) {
    e.preventDefault();
    if (e.dataTransfer.items) {
      for (var i = 0; i < e.dataTransfer.items.length; i++) {
        // If dropped items aren't files, reject them
        if (e.dataTransfer.items[i].kind === "file") {
          let file = e.dataTransfer.items[i].getAsFile();
          this.handleImgUpload(file, true);
        }
      }
    }
    this.dragHover = false;
  }

  sanitize(url: string): any {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
