import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "proportions-section",
  template: `
<div class="section">
  <h1>CONTAINERS</h1>
  <h2>
    A logo often needs to be placed inside areas of varying proportions. Here’s how your logo fits into a few common shapes. 
  </h2>
  <div class="flexCenter">
  <!---Square---->
  <app-card [caption]="'Square'">
    <div class="body" [class.imgFit]="true">
      <div id="square" class="border">
        <img [src]="src">
      </div>
    </div>
  </app-card>
  <!---Wide---->
  <app-card [caption]="'Wide'">
    <div class="body" [class.imgFit]="true">
      <div id="wide" class="border">
        <img [src]="src">
      </div>
    </div>
  </app-card>
  <!---Narrow---->
  <app-card [caption]="'Narrow'">
    <div class="body" [class.imgFit]="true">
      <div id="narrow" class="border">
        <img [src]="src">
      </div>
    </div>
  </app-card>
  </div>
</div>
  `,
  styleUrls: ["./proportions.component.sass"]
})
export class ProportionsComponent {
  @Input()
  src: string;
}
