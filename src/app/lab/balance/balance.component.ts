import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "balance-section",
  template: `
  <div class="section">
  <h1>BALANCE</h1>
  <h2>
    It’s important to maintain visual balance in your logo so that one side doesn’t draw all of the attention.
  </h2>
  <div class="flexCenter">
  <!---Grayscale---->
    <div class="cardCont">
      <div class="card">
          <div class="body"  [class.imgFit]="imgFit">
            <img [src]="src" id="level1">
            <div class="grid"></div>
          </div>
        <div class="shadow"></div>
      </div>
    </div>
  </div>
</div>
  `,
  styleUrls: ["./balance.component.sass"]
})
export class BalanceComponent {
  @Input()
  imgFit: boolean;
  @Input()
  src: string;
}
