import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "colorblind-section",
  template: `
    <div class="section">
      <h1>COLOR BLIND</h1>
      <h2>
        It’s important to remember that not everybody sees the same. This is what your logo looks like to those with color blindness. 
      </h2>
      <div class="flexCenter">
      <app-card [caption]="'Deuteranomaly'">
        <div class="body" [class.imgFit]="imgFit">
          <img [src]="deutSrc" *ngIf="deutSrc; else loading">
        </div>
      </app-card>
      <app-card [caption]="'Protanopia'">
        <div class="body" [class.imgFit]="imgFit">
          <img [src]="protaSrc" *ngIf="protaSrc; else loading">
        </div>
      </app-card>
      <app-card [caption]="'Tritanopia'">
        <div class="body" [class.imgFit]="imgFit">
          <img [src]="tritSrc" *ngIf="tritSrc; else loading">
        </div>
      </app-card>
      </div>
    </div>
    <ng-template #loading>
      <div class="padding">
        <div class="spinner"></div>
      </div>
    </ng-template>
  `,
  styleUrls: ["./colorblind.component.sass"]
})
export class ColorblindComponent {
  img: HTMLImageElement;
  deutSrc: string;
  protaSrc: string;
  tritSrc: string;
  rBlind = {
    protan: {
      cpu: 0.735,
      cpv: 0.265,
      am: 1.273463,
      ayi: -0.073894
    },
    deutan: {
      cpu: 1.14,
      cpv: -0.14,
      am: 0.968437,
      ayi: 0.003331
    },
    tritan: {
      cpu: 0.171,
      cpv: -0.003,
      am: 0.062921,
      ayi: 0.292119
    }
  };
  powGammaLookup;

  @Input()
  imgFit: boolean;
  @Input()
  set setImg(val: HTMLImageElement) {
    if (!this.powGammaLookup) {
      this.powGammaLookup = Array(256);
      for (let i = 0; i < 256; i++) {
        this.powGammaLookup[i] = Math.pow(i / 255, 2.2);
      }
    }
    //reset srcs
    this.deutSrc = null;
    this.protaSrc = null;
    this.tritSrc = null;
    //set img
    this.img = val;
    setTimeout(() => {
      this.createCanvasImages();
    }, 100);
  }

  @Output()
  done = new EventEmitter();

  constructor(private sanitizer: DomSanitizer, private cd: ChangeDetectorRef) {}

  createCanvasImages() {
    let canvas = document.createElement("canvas");
    canvas.height = this.img.height;
    canvas.width = this.img.width;
    //draw original
    let ctx = canvas.getContext("2d");
    ctx.drawImage(this.img, 0, 0);

    //get img data
    let imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    let deutData: any = new ImageData(
      new Uint8ClampedArray(imgData.data),
      imgData.width,
      imgData.height
    );
    let protaData: any = new ImageData(
      new Uint8ClampedArray(imgData.data),
      imgData.width,
      imgData.height
    );
    let tritData: any = new ImageData(
      new Uint8ClampedArray(imgData.data),
      imgData.width,
      imgData.height
    );

    // loop through image data
    for (let row = 0; row < canvas.height; row++) {
      for (let col = 0; col < canvas.width; col++) {
        //find current pixel
        let index = (col + row * imgData.width) * 4;

        //separate into color values
        let rgb = [
          imgData.data[index],
          imgData.data[index + 1],
          imgData.data[index + 2]
        ];
        let a = imgData.data[index + 3];

        //set DEUTDATA
        const deutRGB: any[] = this.anomylize(rgb, this.blindMK(rgb, "deutan"));
        deutData.data[index] = deutRGB[0];
        deutData.data[index + 1] = deutRGB[1];
        deutData.data[index + 2] = deutRGB[2];
        deutData.data[index + 3] = a;

        //set PROTADATA
        const protaRGB: any[] = this.blindMK(rgb, "protan");
        protaData.data[index] = protaRGB[0];
        protaData.data[index + 1] = protaRGB[1];
        protaData.data[index + 2] = protaRGB[2];
        protaData.data[index + 3] = a;

        //set TRITDATA
        const tritRGB: any[] = this.blindMK(rgb, "tritan");
        tritData.data[index] = tritRGB[0];
        tritData.data[index + 1] = tritRGB[1];
        tritData.data[index + 2] = tritRGB[2];
        tritData.data[index + 3] = a;
      }
    }

    // draw new images onto canvas
    //DEUTDATA
    ctx.putImageData(deutData, 0, 0);
    this.deutSrc = this.sanitize(canvas.toDataURL("image/png"));

    //PROTADATA
    ctx.putImageData(protaData, 0, 0);
    this.protaSrc = this.sanitize(canvas.toDataURL("image/png"));

    //TRITDATA
    ctx.putImageData(tritData, 0, 0);
    this.tritSrc = this.sanitize(canvas.toDataURL("image/png"));

    //detect changes
    if (!this.cd["destroyed"]) {
      this.cd.detectChanges();
    }

    //emit done
    this.done.emit();
  }

  multiplyMatrix(rgb, filter) {
    // let result = [];
    // // go through m2 columns
    // for (let x = 0; x < 3; x++) {
    //   result.push(
    //     (rgb[0] * filter[x][0]) / 100 +
    //       (rgb[1] * filter[x][1]) / 100 +
    //       (rgb[2] * filter[x][2]) / 100
    //   );
    // }
    // return result;
    let r = rgb[0];
    let g = rgb[1];
    let b = rgb[2];
    return [
      r * filter.R[0] + g * filter.R[1] + b * filter.R[2],
      r * filter.G[0] + g * filter.G[1] + b * filter.G[2],
      r * filter.B[0] + g * filter.B[1] + b * filter.B[2]
    ];
  }

  sanitize(url: string): any {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  blindMK(a, b) {
    var d = 0.312713,
      e = 0.329016,
      f = 0.358271,
      g = a[2], //BLUE
      h = a[1], //GREEN
      i = a[0], //RED
      j = this.powGammaLookup[i],
      k = this.powGammaLookup[h],
      l = this.powGammaLookup[g],
      m = 0.430574 * j + 0.34155 * k + 0.178325 * l,
      n = 0.222015 * j + 0.706655 * k + 0.07133 * l,
      o = 0.020183 * j + 0.129553 * k + 0.93918 * l,
      p = m + n + o,
      q = 0,
      r = 0;
    0 != p && ((q = m / p), (r = n / p));
    var u,
      s = (d * n) / e,
      t = (f * n) / e,
      v = 0;
    u =
      q < this.rBlind[b].cpu
        ? (this.rBlind[b].cpv - r) / (this.rBlind[b].cpu - q)
        : (r - this.rBlind[b].cpv) / (q - this.rBlind[b].cpu);
    var w = r - q * u,
      x = (this.rBlind[b].ayi - w) / (u - this.rBlind[b].am),
      y = u * x + w,
      z = (x * n) / y,
      A = n,
      B = ((1 - (x + y)) * n) / y,
      C = 3.063218 * z - 1.393325 * A - 0.475802 * B,
      D = -0.969243 * z + 1.875966 * A + 0.041555 * B,
      E = 0.067871 * z - 0.228834 * A + 1.069251 * B,
      F = s - z,
      G = t - B,
      dr = 3.063218 * F - 1.393325 * v - 0.475802 * G,
      dg = -0.969243 * F + 1.875966 * v + 0.041555 * G,
      db = 0.067871 * F - 0.228834 * v + 1.069251 * G;
    var H = dr ? ((C < 0 ? 0 : 1) - C) / dr : 0,
      I = dg ? ((D < 0 ? 0 : 1) - D) / dg : 0,
      J = db ? ((E < 0 ? 0 : 1) - E) / db : 0,
      K = Math.max(
        H > 1 || H < 0 ? 0 : H,
        I > 1 || I < 0 ? 0 : I,
        J > 1 || J < 0 ? 0 : J
      );
    return (
      (C += K * dr),
      (D += K * dg),
      (E += K * db),
      [this.inversePow(C), this.inversePow(D), this.inversePow(E)]
    );
  }

  inversePow(a) {
    return 255 * (a <= 0 ? 0 : a >= 1 ? 1 : Math.pow(a, 1 / 2.2));
  }

  anomylize(a, b) {
    let c = 1.75,
      d = 1 * c + 1;
    return [
      (c * b[0] + 1 * a[0]) / d,
      (c * b[1] + 1 * a[1]) / d,
      (c * b[2] + 1 * a[2]) / d
    ];
  }
}
