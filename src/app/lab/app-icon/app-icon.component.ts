import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "app-icon-section",
  template: `
  <div class="section">
  <h1>APP ICON</h1>
  <h2>
  This is what your logo would look like as an icon on a mobile device. 
  </h2>
  <div class="flexCenter">
    <div class="wrapper">
      <div class="phone">
        <img src="/assets/lightPhone.png">
        <div class="icon" id="light">
          <img [src]="src">
        </div>
        <div class="shadow"></div>
      </div>
      <div class="phone">
        <img src="/assets/darkPhone.png">
        <div class="icon" id="dark">
           <img [src]="src">
        </div>
        <div class="shadow"></div>
      </div>
    </div>
  </div>
</div>
  `,
  styleUrls: ["./app-icon.component.sass"]
})
export class AppIconComponent {
  @Input()
  src: string;
}
