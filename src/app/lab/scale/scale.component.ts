import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "scale-section",
  template: `
  <div class="section">
  <h1>SCALABILITY</h1>
  <h2>
    A logo needs to work at many different sizes, from really big to really small.
  </h2>
  <div class="flexCenter">
  <!---Grayscale---->
  <app-card [caption]="'Medium'">
    <div class="body" [class.imgFit]="imgFit">
      <img [src]="src" id="small">
    </div>
  </app-card>
  <!---Black---->
  <app-card [caption]="'Small'">
    <div class="body" [class.imgFit]="imgFit">
      <img [src]="src" id="smaller">
    </div>
  </app-card>
  <!---White---->
  <app-card [caption]="'Tiny'">
    <div class="body"  [class.imgFit]="imgFit">
      <img [src]="src" id="smallest">
    </div>
  </app-card>
  </div>
</div>
  `,
  styleUrls: ["./scale.component.sass"]
})
export class ScaleComponent {
  @Input()
  imgFit: boolean;
  @Input()
  src: string;
}
