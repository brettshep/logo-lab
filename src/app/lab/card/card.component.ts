import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "app-card",
  template: `
    <figure>
      <div class="card">
          <ng-content select = ".body"></ng-content>
        <div class="shadow"></div>
      </div>
      <ng-content select = "figcaption"></ng-content>
      <figcaption>{{caption}}</figcaption>
    </figure>
  `,
  styleUrls: ["./card.component.sass"]
})
export class CardComponent {
  @Input()
  caption: string;
}
