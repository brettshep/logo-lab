import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { LabComponent } from './lab.component';
import { ColorblindComponent } from './colorblind/colorblind.component';
import { CardComponent } from './card/card.component';
import { ColorComponent } from './color/color.component';
import { ScaleComponent } from './scale/scale.component';
import { PixelateComponent } from './pixelate/pixelate.component';
import { BlurComponent } from './blur/blur.component';
import { BalanceComponent } from './balance/balance.component';
import { SlicesComponent } from './slices/slices.component';
import { GridComponent } from './grid/grid.component';
import { ProportionsComponent } from './proportions/proportions.component';
import { AppIconComponent } from './app-icon/app-icon.component';

export const ROUTES: Routes = [
  {
    path: '',
    component: LabComponent,
  },
  { path: '**', redirectTo: '/home' },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  declarations: [
    LabComponent,
    ColorblindComponent,
    CardComponent,
    ColorComponent,
    ScaleComponent,
    PixelateComponent,
    BlurComponent,
    BalanceComponent,
    SlicesComponent,
    GridComponent,
    ProportionsComponent,
    AppIconComponent,
  ],
})
export class LabModule {}
