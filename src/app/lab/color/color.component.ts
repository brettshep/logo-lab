import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "color-section",
  template: `
<div class="section">
  <h1>COLOR</h1>
  <h2>
    A hallmark of a great logo is that it works not only in color, but also in black and white. 
  </h2>
  <div class="flexCenter">
  <!---Grayscale---->
  <app-card [caption]="'Grayscale'">
    <div class="body" [class.imgFit]="imgFit">
      <img [src]="src" id="colGrayscale">
    </div>
  </app-card>
  <!---Black---->
  <app-card [caption]="'Black'">
    <div class="body" [class.imgFit]="imgFit">
      <img [src]="src" id="colBlack">
    </div>
  </app-card>
  <!---White---->
  <app-card [caption]="'White'">
    <div class="body" id="colWhite" [class.imgFit]="imgFit">
      <img [src]="src">
    </div>
  </app-card>
  </div>
</div>
  `,
  styleUrls: ["./color.component.sass"]
})
export class ColorComponent {
  @Input()
  imgFit: boolean;
  @Input()
  src: string;
}
