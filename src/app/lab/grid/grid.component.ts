import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "grid-section",
  template: `
  <div class="section">
  <h1>ATTENTION</h1>
  <h2>
  A logo is always competing for the attention of the viewer. How does yours standout in a crowd of familiar brands?
  </h2>
  <div class="flexCenter">
  <!---Grayscale---->
    <div class="cardCont">
      <div class="card">
          <div class="body">
            <div class="grid">
                <img [src]="image" *ngFor="let image of images">
            </div>
          </div>
        <div class="shadow"></div>
      </div>
    </div>
  </div>
</div>
  `,
  styleUrls: ["./grid.component.sass"]
})
export class GridComponent {
  @Input()
  imgFit: boolean;
  @Input()
  set setSrc(val: string) {
    this.images = this.shuffleArray([...this.baseImages, val]);
  }
  //11 base images
  baseImages: string[] = [
    "/assets/logos/cocacola.png",
    "/assets/logos/dropbox.png",
    "/assets/logos/facebook.png",
    "/assets/logos/firefox.png",
    "/assets/logos/google.png",
    "/assets/logos/nike.png",
    "/assets/logos/shell.png",
    "/assets/logos/target.png",
    "/assets/logos/twitter.png",
    "/assets/logos/instagram.png",
    "/assets/logos/spotify.png"
  ];
  images: string[] = [];

  shuffleArray(array: any[]) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return [...array];
  }
}
