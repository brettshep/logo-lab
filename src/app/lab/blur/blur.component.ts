import { Component, ChangeDetectionStrategy, Input } from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "blur-section",
  template: `
  <div class="section">
  <h1>BLUR</h1>
  <h2>
    Blur helps you see the main forms in your logo. This shows what stands out to someone on a first glance or if they are just passing by it quickly. 
  </h2>
  <div class="flexCenter">
  <!---Grayscale---->
  <app-card [caption]="'Level 1'">
    <div class="body" [class.imgFit]="imgFit">
      <img [src]="src" id="level1">
    </div>
  </app-card>
  <!---Black---->
  <app-card [caption]="'Level 2'">
    <div class="body" [class.imgFit]="imgFit">
      <img [src]="src" id="level2">
    </div>
  </app-card>
  <!---White---->
  <app-card [caption]="'Level 3'">
    <div class="body"  [class.imgFit]="imgFit">
      <img [src]="src" id="level3">
    </div>
  </app-card>
  </div>
</div>
  `,
  styleUrls: ["./blur.component.sass"]
})
export class BlurComponent {
  @Input()
  imgFit: boolean;
  @Input()
  src: string;
}
