import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'pixelate-section',
  template: `
    <div class="section">
      <h1>PIXELATED</h1>
      <h2>
        At low resolutions, logos begin losing detail. A good logo retains most
        of its form at low resolutions.
      </h2>
      <div class="flexCenter">
        <ng-container *ngFor="let level of levels">
          <app-card [caption]="level.name">
            <div class="body" [class.imgFit]="imgFit">
              <img [src]="level.src" *ngIf="level.src; else loading" />
            </div>
          </app-card>
        </ng-container>
      </div>
    </div>
    <ng-template #loading>
      <div class="padding">
        <div class="spinner"></div>
      </div>
    </ng-template>
  `,
  styleUrls: ['./pixelate.component.sass'],
})
export class PixelateComponent {
  img: HTMLImageElement;
  levels = [
    { src: null, size: 0.21333, name: null },
    { src: null, size: 0.10666, name: null },
    { src: null, size: 0.07111, name: null },
  ];

  @Input()
  imgFit: boolean;
  @Input()
  set setImg(val: HTMLImageElement) {
    //reset srcs
    this.levels = this.levels.map((level) => {
      level.src = null;
      return level;
    });
    //set img
    this.img = val;
    setTimeout(() => {
      this.createCanvasImages();
    }, 100);
  }

  @Output()
  done = new EventEmitter();

  constructor(private sanitizer: DomSanitizer, private cd: ChangeDetectorRef) {}

  createCanvasImages() {
    //create and setup canvas
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    let canvasTiny = document.createElement('canvas');
    let ctxTiny = canvasTiny.getContext('2d');
    //setting height/width turns on antialias
    canvas.height = this.img.height;
    canvas.width = this.img.width;

    //turn off antialias
    (ctx as any).mozImageSmoothingEnabled = false;
    (ctx as any).webkitImageSmoothingEnabled = false;
    ctx.imageSmoothingEnabled = false;

    //get small size
    this.levels = this.levels.map((level) => {
      let w = canvas.width * level.size;
      let h = canvas.height * level.size;

      //set name using pixel size
      level.name = `${Math.round(canvas.width * level.size)}px × ${Math.round(
        canvas.height * level.size
      )}px`;

      //fill tinycanvas white and draw img
      ctxTiny.fillStyle = 'white';
      ctxTiny.fillRect(0, 0, canvasTiny.width, canvasTiny.height);
      ctxTiny.drawImage(this.img, 0, 0, w, h);

      // //draw tiny img to canvas full size
      ctx.drawImage(canvasTiny, 0, 0, w, h, 0, 0, canvas.width, canvas.height);

      //set src
      level.src = this.sanitize(canvas.toDataURL('image/png'));

      //return level
      return level;
    });

    //detect changes
    if (!this.cd['destroyed']) {
      this.cd.detectChanges();
    }

    //emit done
    this.done.emit();
  }

  sanitize(url: string): any {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
