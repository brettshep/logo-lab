import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "slices-section",
  template: `
  <div class="section">
  <h1>SLICES</h1>
  <h2>
    It’s an extra plus if your logo is still recognizable even when only seeing a small part of it.
  </h2>
  <div class="flexCenter">
  <ng-container *ngFor="let slice of slices">
    <div class="cardCont">
      <div class="card">
        <div class="body" [class.imgFit]="true">
          <img [src]="slice" *ngIf="slice; else loading">
        </div>
        <div class="shadow"></div>
      </div>
    </div>
  </ng-container>
  </div>
</div>
<ng-template #loading>
  <div class="padding">
    <div class="spinner"></div>
  </div>
</ng-template>
  `,
  styleUrls: ["./slices.component.sass"]
})
export class SlicesComponent {
  img: HTMLImageElement;
  aspectThresh = 2;
  slices: any[] = [null, null, null, null];
  @Input()
  set setImg(val: HTMLImageElement) {
    //reset slices
    this.slices = this.slices.map(slice => {
      return null;
    });
    //set img
    this.img = val;
    setTimeout(() => {
      this.createCanvasImages();
    }, 100);
  }

  @Output()
  done = new EventEmitter();

  constructor(private sanitizer: DomSanitizer, private cd: ChangeDetectorRef) {}

  createCanvasImages() {
    //create and setup canvas
    let canvas = document.createElement("canvas");
    let ctx = canvas.getContext("2d");

    let horizSliceOnly = this.img.width / this.img.height >= this.aspectThresh;
    if (horizSliceOnly) {
      let slice = { w: this.img.width / 4, h: this.img.height };
      //setting height/width
      canvas.width = slice.w;
      canvas.height = slice.h;
      this.slices = this.slices.map((val, index) => {
        //clear canvas
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        //draw image slice
        ctx.drawImage(
          this.img,
          0 + slice.w * index,
          0,
          slice.w,
          slice.h,
          0,
          0,
          canvas.width,
          canvas.height
        );
        return this.sanitize(canvas.toDataURL("image/png"));
      });
    } else {
      let slice = { w: this.img.width / 2, h: this.img.height / 2 };
      //setting height/width
      canvas.width = slice.w;
      canvas.height = slice.h;
      this.slices = this.slices.map((val, index) => {
        //clear canvas
        ctx.clearRect(0, 0, slice.w, slice.h);

        //draw image slice
        ctx.drawImage(
          this.img,
          0 + slice.w * (index % 2),
          0 + slice.h * (index > 1 ? 1 : 0),
          slice.w,
          slice.h,
          0,
          0,
          canvas.width,
          canvas.height
        );
        return this.sanitize(canvas.toDataURL("image/png"));
      });
    }

    //shuffles array
    this.slices = this.shuffleArray(this.slices);

    //detect changes
    if (!this.cd["destroyed"]) {
      this.cd.detectChanges();
    }

    //emit done
    this.done.emit();
  }

  sanitize(url: string): any {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  shuffleArray(array: number[]) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return [...array];
  }
}
