import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";
import { Store } from "./store";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class LabGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.select("origFile").pipe(
      map(val => {
        if (val) {
          return true;
        } else {
          this.router.navigate([`/home`]);
          return false;
        }
      })
    );
  }
}
